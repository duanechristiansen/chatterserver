# ChatterServer

Swift-Vapor chat server as proof of concept

This project includes a `docker-compose.yml` file for reference on how to get the container up-and-running.

**NOTE** This docker-compose configuration is not suitable for production deployment and is only for testing

To run:
  - clone the repo
  - `docker-compose up -d app`
  - Check the logs by running `docker logs <container_id>`
  - If logs indicate databse requires initializing, run `docker-compose run migrate` to set up the tables
  - visit [localhost:8080](http://localhost:8080/) to see the admin console (where you answer any questions sent in)
  - Make a web socket connection to [ws://localhost:8080/socket](ws://localhost:8080/socket) to communicate with the server.

Once connected, the server will provide the handshake response winth an id to be sent with subsequent communications during the session:
```json
{
    "id": <GUID>,
    "type": "handshake"
}
```

  WS REQUEST:
  ```json
{
    "id": <received_from_server_in_handshake_response>,
    "type": "newQuestion",
    "content": "My awesome question goes here"
}
  ```

  SERVER RESPONSE:
  ```json
{
    "questionId": <GUIID>,
    "response": 
        {
            "answer":"Here is my awesome answer!"
        },
    "type": "questionAnswer"
}
  ```

All data is backed by a Postgres DB that is stood up by docker-compose (see docker-compose.yml for details).  If running this container without the provided docker-compose.yml then you would need to connect the database manually.