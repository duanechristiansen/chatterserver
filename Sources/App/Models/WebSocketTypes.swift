//
//  File.swift
//  
//
//  Created by Duane Christiansen on 2/27/22.
//

import Foundation

enum MessageType: String, Codable {
    // Client to server types
    case newQuestion
    // Server to client types
    case questionResponse, handshake, questionAnswer
}

struct MessageData: Codable {
    let type: MessageType
    let id: UUID
}

struct Handshake: Codable {
    var type = MessageType.handshake
    let id: UUID
}

struct QuestionModel {
    struct Request: Codable {
        let content: String
    }
    
    struct Response: Codable {
        var type = MessageType.questionResponse
        let success: Bool
        let message: String
        let id: UUID
        let answer: String?
        let content: String
        let createdAt: Date?
    }
}


struct Answer: Codable {
    struct Response: Codable {
        let value: String
        
        enum CodingKeys: String, CodingKey {
            case value = "answer"
        }
    }
    var type = MessageType.questionAnswer
    let questionId: UUID
    let response: Response?
}
