import Fluent

struct CreateQuestion: AsyncMigration {
    func prepare(on database: Database) async throws {
        try await database.schema(Question.schema)
            .id()
            .field("content", .string, .required)
            .field("answer", .string)
            .field("asked_from", .uuid, .required)
            .field("created_at", .date)
            .create()
    }

    func revert(on database: Database) async throws {
        try await database.schema(Question.schema).delete()
    }
}
