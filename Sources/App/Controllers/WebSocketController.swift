import Vapor
import Fluent

enum WebSocketSendOption {
    case id(UUID), socket(WebSocket)
    case all, ids([UUID])
}

class WebSocketController {
    let lock: Lock
    var sockets: [UUID: WebSocket]
    let db: Database
    let logger: Logger
    
    init(db: Database) {
        self.lock = Lock()
        self.sockets = [:]
        self.db = db
        self.logger = Logger(label: "WebSocketController")
    }
    
    func connect(_ ws: WebSocket) {
        
        let uuid = UUID()
        self.lock.withLockVoid {
            self.sockets[uuid] = ws
        }
        
        ws.onBinary { [weak self] ws, buffer in
            guard let self = self,
                  let data = buffer.getData(
                    at: buffer.readerIndex, length: buffer.readableBytes) else {
                        return
                    }
            
            self.onData(ws, data)
        }
        
        ws.onText { [weak self] ws, text in
            guard let self = self,
                  let data = text.data(using: .utf8) else {
                      return
                  }
            
            self.onData(ws, data)
        }
        
        self.send(message: Handshake(id: uuid), to: .socket(ws))
        
    }
    
    func send<T: Codable>(message: T, to sendOption: WebSocketSendOption) {
        logger.info("Sending \(T.self) to \(sendOption)")
        do {
            let sockets: [WebSocket] = self.lock.withLock {
                switch sendOption {
                case .id(let id):
                    return [self.sockets[id]].compactMap { $0 }
                case .socket(let socket):
                    return [socket]
                case .all:
                    return self.sockets.values.map { $0 }
                case .ids(let ids):
                    return self.sockets.filter { key, _ in ids.contains(key) }.map { $1 }
                }
            }
            
            let encoder = JSONEncoder()
            let data = try encoder.encode(message)
            
            sockets.forEach {
                $0.send(raw: data, opcode: .binary)
            }
        } catch {
            logger.report(error: error)
        }
    }
    
    func onData(_ ws: WebSocket, _ data: Data) {
        let decoder = JSONDecoder()
        do {
            let messageData = try decoder.decode(MessageData.self, from: data)
            switch messageData.type {
            case .newQuestion:
                let newQuestionData = try decoder.decode(QuestionModel.Request.self, from: data)
                self.onNewQuestion(ws, messageData.id, newQuestionData)
            default: break
            }
        } catch {
            logger.report(error: error)
        }
    }
    
    func onNewQuestion(_ ws: WebSocket, _ id: UUID, _ message: QuestionModel.Request) {
        let q = Question(content: message.content, askedFrom: id)
        self.db.withConnection {
            q.save(on: $0)
        }.whenComplete { res in
            let success: Bool
            let message: String
            switch res {
            case .failure(let err):
                self.logger.report(error: err)
                success = false
                message = "Something went wrong creating the question."
            case .success:
                self.logger.info("Got a new question!")
                success = true
                message = "Question created. We will answer it as soon as possible :]"
            }
            
            try? self.send(message: QuestionModel.Response(
                success: success,
                message: message,
                id: q.requireID(),
                answer: q.answer,
                content: q.content,
                createdAt: q.createdAt
            ), to: .socket(ws))
        }
    }
}
