import Fluent
import Vapor
import Leaf

struct QuestionsController: RouteCollection {
    let wsController: WebSocketController
    
    // MARK: - Routes
    func boot(routes: RoutesBuilder) throws {
        // WebSocket
        routes.webSocket("socket", onUpgrade: self.webSocket)
        
        // Index
        routes.get(use: index)
        
        // Answer
        routes.post(":questionId", "answer", use: answer)
        
    }
    
    // MARK: - WebSocket
    func webSocket(req: Request, socket: WebSocket) {
        self.wsController.connect(socket)
    }
    
    // MARK: - Index
    struct QuestionsContext: Encodable {
        let questions: [Question]
    }
    
    func index(req: Request) throws -> EventLoopFuture<View> {
        Question.query(on: req.db).all().flatMap {
            return req.view.render("questions", QuestionsContext(questions: $0))
        }
    }
    // NOTE: Scheme update for using Leaf:
    //  - CMD-OPT-R
    //  - Options -> Working Directory
    //  - Use custom working directlory
    //      - Profile: (/Users/duane/Desktop/Chatter/websockets-backend)
    //      - Also add to Run - Working Directory
    
    
    func answer(req: Request) throws -> EventLoopFuture<Response> {
        guard let questionId = req.parameters.get("questionId"),
              let answer = try? req.content.decode(Answer.Response.self),
              let questionUid = UUID(questionId) else {
                  throw Abort(.badRequest)
              }
        return Question.find(questionUid, on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { question in
                // Update local model
                question.answer = answer.value
                return question.save(on: req.db).flatMapThrowing {
                    // Send result to client
                    let message = try Answer(questionId: question.requireID(), response: answer)
                    self.wsController.send(message: message, to: .id(question.askedFrom))
                    return req.redirect(to: "/")
                }
            }
    }
    
}
