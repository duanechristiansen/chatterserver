import Fluent
import Leaf
import FluentPostgresDriver
import Vapor

// configures your application
public func configure(_ app: Application) throws {
    // Configure database
    app.databases.use(.postgres(
        hostname: Environment.get("DATABASE_HOST") ?? "localhost",
        port: Environment.get("DATABASE_PORT").flatMap(Int.init(_:)) ?? PostgresConfiguration.ianaPortNumber,
        username: Environment.get("DATABASE_USERNAME") ?? "vapor_username",
        password: Environment.get("DATABASE_PASSWORD") ?? "vapor_password",
        database: Environment.get("DATABASE_NAME") ?? "vapor_database"
    ), as: .psql)

    app.migrations.add(CreateQuestion())
    
    // Run migrations at app startup.
    try app.autoMigrate().wait()
    
    // Configure rendering engine
    app.views.use(.leaf)
    
    // register routes
    try routes(app)
}
